﻿using System.Collections;
using UnityEngine;
using DUtils;

public class ExampleTranslate : UILerpAnimationBase.Templete<Vector3>
{
	[SerializeField]
	private Transform _source;

	[SerializeField]
	private Transform _destination;

	[SerializeField]
	private Transform _target;

	protected override void OnBegin(Vector3 from, bool isForward)
	{
		_target.position = isForward ? _source.position : _destination.position;
	}

	protected override void OnTransition(Vector3 from, Vector3 to, float alpha)
	{
		_target.position = Vector3.Lerp(from, to, alpha);
	}

	protected override void OnEnd(Vector3 to, bool isForward)
	{
		_target.position = !isForward ? _source.position : _destination.position;
	}

	protected override Vector3 GetFrom() => _source.transform.position;

	protected override Vector3 GetTo() => _target.transform.position;
}

public abstract class UILerpAnimationBase : MonoBehaviour
{
	public AnimationType AniType;

	[SerializeField]
	protected float AniTime = 0.7f;

	[SerializeField]
	protected AnimationCurve Curve;

	private bool _isInitalized;

	public enum AnimationType
	{
		ToTarget,
		ToOrigin
	}

	public virtual void Play(bool playForward, bool skipAnimation)
	{
		OnPlay(playForward, skipAnimation);
	}

	protected virtual void Awake()
	{
		_isInitalized = true;
		if (Curve.keys.Length == 0)
		{
			Curve.AddKey(new Keyframe(0f, 0f, 4.85f, 4.85f));
			Curve.AddKey(new Keyframe(0.15f, 0.729f, 1.489f, 1.160f));
			Curve.AddKey(new Keyframe(1f, 1f, 0f, 0f));
		}
	}

	protected virtual void Start()
	{
	}

	protected abstract void OnPlay(bool playForward, bool skipAnimation);

	public abstract class Templete<T> : UILerpAnimationBase
	{
		private ICoroutineBinder _sequence;

		protected abstract void OnBegin(T from, bool isPlayForward);

		protected abstract void OnTransition(T from, T to, float alpha);

		protected abstract void OnEnd(T to, bool isPlayForward);

		protected abstract T GetFrom();

		protected abstract T GetTo();

		protected sealed override void OnPlay(bool isPlayForward, bool skipAnimation)
		{
			if (skipAnimation || !gameObject.activeInHierarchy)
			{
				if (!_isInitalized)
				{
					Awake();
				}

				if (AniType == AnimationType.ToTarget)
				{
					OnEnd(isPlayForward ? GetTo() : GetFrom(), isPlayForward);
				}
				else if (AniType == AnimationType.ToOrigin)
				{
					OnEnd(isPlayForward ? GetFrom() : GetTo(), isPlayForward);
				}
			}
			else
			{
				this.StartCoroutine(ref this._sequence, FillSequence(isPlayForward));
			}
		}

		private IEnumerator FillSequence(bool isPlayForward)
		{
			T from = isPlayForward ? GetFrom() : GetTo();
			T to = isPlayForward ? GetTo() : GetFrom();

			OnBegin(from, isPlayForward);

			for (float time = 0; time < AniTime; time += Time.deltaTime)
			{
				OnTransition(from, to, Curve.Evaluate(time / AniTime));
				yield return null;
			}

			if (AniType == AnimationType.ToTarget)
			{
				OnEnd(to, isPlayForward);
			}
			else if (AniType == AnimationType.ToOrigin)
			{
				OnEnd(from, isPlayForward);
			}
		}
	}
}
