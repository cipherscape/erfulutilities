﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
static class EditorMenus
{
    // taken from: http://answers.unity3d.com/questions/282959/set-inspector-lock-by-code.html
    [MenuItem("Shortcuts/Toggle Inspector Lock %h")] // Ctrl + L
    static void ToggleInspectorLock()
    {
        ActiveEditorTracker.sharedTracker.isLocked = !ActiveEditorTracker.sharedTracker.isLocked;
        ActiveEditorTracker.sharedTracker.ForceRebuild();
    }

    [MenuItem("Shortcuts/ActiveToggle %g")]
    static void ToggleActivationSelection()
    {
        var go = Selection.activeGameObject;
        go.SetActive(!go.activeSelf);
    }

	[MenuItem("Shortcuts/Apply Prefab Changes %#g")]
    static public void applyPrefabChanges()
    {
        var obj = Selection.activeGameObject;
        if (obj != null)
        {
            var prefab_root = PrefabUtility.FindPrefabRoot(obj);
            var prefab_src = PrefabUtility.GetCorrespondingObjectFromSource(prefab_root);
            if (prefab_src != null)
            {
                PrefabUtility.ReplacePrefab(prefab_root, prefab_src, ReplacePrefabOptions.ConnectToPrefab);
            }
        }
    }

	[MenuItem("Shortcuts/Run _F5")]
	static void PlayGame()
	{
		EditorSceneManager.SaveScene(SceneManager.GetActiveScene(), "", false);
		EditorApplication.ExecuteMenuItem("Edit/Play");
	}

	[MenuItem("Shortcuts/Pause #_F5")]
	static void PauseGame()
	{
		Debug.Break();
	}

	[MenuItem("Shortcuts/OpenDataSourceFolder %#u")] // Ctrl + Shift
	static void OpenDataExcelSourceFolder()
	{
		DebugUtils.OpenInFileBrowser(Application.dataPath + "/DataTables~");
	}

}
#endif
