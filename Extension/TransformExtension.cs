﻿using UnityEngine;

public static class TransformExtension
{
    public static void DebugRay(this Transform value, Vector3 normal, Color color)
    {
        Debug.DrawRay(value.position, normal.normalized, color);
    }

    public static void DebugRay(this Transform value, Vector3 normal, Color color, float duration)
    {
        Debug.DrawRay(value.position, normal.normalized, color, duration);
    }
}