using UnityEngine;

public static class VectorExtensionMethods
{
    public static Vector2 xy(this Vector3 v)
    {
        return new Vector2(v.x, v.y);
    }

    public static Vector3 WithX(this Vector3 v, float x)
    {
        return new Vector3(x, v.y, v.z);
    }

    public static Vector3 WithY(this Vector3 v, float y)
    {
        return new Vector3(v.x, y, v.z);
    }

    public static Vector3 WithZ(this Vector3 v, float z)
    {
        return new Vector3(v.x, v.y, z);
    }

    public static Vector2 WithX(this Vector2 v, float x)
    {
        return new Vector2(x, v.y);
    }

    public static Vector2 WithY(this Vector2 v, float y)
    {
        return new Vector2(v.x, y);
    }

    public static Vector3 WithZ(this Vector2 v, float z)
    {
        return new Vector3(v.x, v.y, z);
    }

    // axisDirection - unit vector in direction of an axis (eg, defines a line that passes through zero)
    // ConerPoint - the ConerPoint to find nearest on line for
    public static Vector3 NearestConerPointOnAxis(this Vector3 axisDirection, Vector3 ConerPoint, bool isNormalized = false)
    {
        if (!isNormalized) axisDirection.Normalize();
        var d = Vector3.Dot(ConerPoint, axisDirection);
        return axisDirection * d;
    }

    // lineDirection - unit vector in direction of line
    // ConerPointOnLine - a ConerPoint on the line (allowing us to define an actual line in space)
    // ConerPoint - the ConerPoint to find nearest on line for
    public static Vector3 NearestConerPointOnLine(
        this Vector3 lineDirection, Vector3 ConerPoint, Vector3 ConerPointOnLine, bool isNormalized = false)
    {
        if (!isNormalized) lineDirection.Normalize();
        var d = Vector3.Dot(ConerPoint - ConerPointOnLine, lineDirection);
        return ConerPointOnLine + (lineDirection * d);
    }
}