﻿using UnityEngine;

public static class PhysicsExt
{
	/// <summary>
	/// {downward gravity < 0}
	/// </summary>
	public static Vector3 MuzzleVelocity2D(Vector3 direction, float angle, float gravirtyForce)
	{
		float h = direction.y;                                            // get height difference
		direction.y = 0;                                                // remove height
		float distance = direction.magnitude;                            // get horizontal distance
		float a = angle * Mathf.Deg2Rad;                                // Convert angle to radians
		direction.y = distance * Mathf.Tan(a);                            // Set direction to elevation angle
		distance += h / Mathf.Tan(a);                                        // Correction for small height differences

		// calculate velocity
		float velocity = Mathf.Sqrt(distance * Mathf.Abs(gravirtyForce) / Mathf.Sin(2 * a));
		return velocity * direction.normalized;
	}

	public static void RotateToDirection2D(Transform f_trf, Vector2 f_dir)
	{
		f_trf.rotation = Quaternion.identity;
		f_trf.Rotate(Vector3.forward, Mathf.Atan2(f_dir.y, f_dir.x) * Mathf.Rad2Deg);
	}
}
