﻿using System.Collections.Generic;
using System.Linq;

public static class CollectionParser
{
	public static string ToReadableText<TKey, TValue>(this IDictionary<TKey, TValue> dictionary)
	{
		return "{" + string.Join(",", dictionary.Select(kv => kv.Key + "=" + kv.Value).ToArray()) + "}";
	}
}