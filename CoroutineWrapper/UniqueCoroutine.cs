﻿using JetBrains.Annotations;
using System.Collections;
using UnityEngine;
using ZUtil.CoroutineBinderHelper;

namespace ZUtil.CoroutineBinderHelper
{
	public class CoroutineBinder : ICoroutineBinder
	{
		public IEnumerator CachedCoroutine;
		private readonly MonoBehaviour _coroutineOwner;

		public CoroutineBinder(MonoBehaviour owner)
		{
			_coroutineOwner = owner;
		}

		public void Stop()
		{
			if (CachedCoroutine != null)
			{
				_coroutineOwner.StopCoroutine(CachedCoroutine);
				CachedCoroutine = null;
			}
		}

		public bool TryReapply(IEnumerator newCoroutine)
		{
			if (_coroutineOwner == null)
			{
				Debug.LogWarning("Object already destroyed");
				return false;
			}
			Stop();
			CachedCoroutine = newCoroutine;
			_coroutineOwner.StartCoroutine(newCoroutine);
			return true;
		}
	}

	public class CoroutineMananger : SingletonObject<CoroutineMananger>
	{
	}
}

public interface ICoroutineBinder
{
}

public static class UniqueCoroutineExtension
{
	/// <summary>
	///     코루틴을 캐시하여, 하나의 코루틴만 있도록 유지하고 원하는 타이밍에 정지할 수 있도록 합니다.
	/// </summary>
	public static Coroutine StartCoroutine(this MonoBehaviour owner, [CanBeNull] ref ICoroutineBinder binder,
		[NotNull] IEnumerator coroutine)
	{
		if (owner == null)
		{
			Debug.LogWarning("Object already destroyed");
			return null;
		}

		if (binder == null)
		{
			binder = new CoroutineBinder(owner);
		}

		var safeCoroutineBinder = (CoroutineBinder)binder;

		safeCoroutineBinder.Stop();
		safeCoroutineBinder.CachedCoroutine = coroutine;
		return owner.StartCoroutine(safeCoroutineBinder.CachedCoroutine);
	}

	public static Coroutine StartCoroutine([CanBeNull] ref ICoroutineBinder binder, [NotNull] IEnumerator coroutine)
	{
		return StartCoroutine(CoroutineMananger.Ins, ref binder, coroutine);
	}

	public static void StopCoroutine(this MonoBehaviour owner, ICoroutineBinder binder)
	{
		var sequence = binder as CoroutineBinder;
		if (sequence != null)
		{
			sequence.Stop();
		}
	}

	public static void StopCoroutine(ICoroutineBinder binder)
	{
		StopCoroutine(CoroutineMananger.Ins, binder);
	}
}