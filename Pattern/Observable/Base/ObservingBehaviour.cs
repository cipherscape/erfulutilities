﻿using ZUtil.ObservingBehaviourInternal;
using System.Collections.Generic;

namespace ZUtil.ObservingBehaviourInternal
{
	using UnityEngine;

	public class ObservingBehaviourCore : MonoBehaviour
	{
		protected virtual void OnDisable()
		{
		}

		protected virtual void Update()
		{
		}
	}
}

public class ObservingBehaviour : ObservingBehaviourCore
{
	private List<IMonoObservable> _everyUpdateSubscriptions = new List<IMonoObservable>();
	private List<IMonoObservable> _plainSubscriptions = new List<IMonoObservable>();

	protected ObservingBehaviour Bind<T>(PlainObservable<T> data, ObservableBase<T>.Handler callback)
	{
		_plainSubscriptions.Add(data);
		data.Subscribe(gameObject, callback);
		return this;
	}

	protected ObservingBehaviour Bind<T>(UpdateObservable<T> data, ObservableBase<T>.Handler callback)
	{
		_plainSubscriptions.Add(data);
		data.Subscribe(gameObject, callback);
		return this;
	}

	protected virtual void Disabled()
	{
	}

	protected sealed override void OnDisable()
	{
		PerfectClaer(_plainSubscriptions);
		PerfectClaer(_everyUpdateSubscriptions);
		Disabled();
	}

	protected sealed override void Update()
	{
		for (int i = 0; i < _plainSubscriptions.Count; i++)
		{
			_plainSubscriptions[i].Notify();
		}
		Updated();
	}

	protected virtual void Updated()
	{
	}

	private void PerfectClaer(List<IMonoObservable> observers)
	{
		for (int i = 0; i < observers.Count; i++)
		{
			observers[i].UnsubscribeAll();
		}
		observers.Clear();
	}
}