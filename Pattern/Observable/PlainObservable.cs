﻿public class PlainObservable<T> : ObservableBase<T>
{
    public PlainObservable(T init = default(T)) : base()
    {
        _value = init;
    }

    public override void Notify()
    {
        base.Notify();
    }

    protected override void NotifyInternal()
    {
        return;
    }
}